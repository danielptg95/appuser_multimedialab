import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import { Button } from "reactstrap";

import ModalRegister from "../components/ModalRegister";
import LogoImage from "../logo.svg";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mostrar: false,
      user: {
        nombre: "",
        usuario: "",
        clave: "",
        correo: "",
      },
    };
  }

  mostrarModal = () => {
    this.setState({ mostrar: !this.state.mostrar });
  };

  componentDidMount() {
    let ds_user = "user_" + this.props.match.params.nombre;
    let JSON_User = JSON.parse(localStorage.getItem(ds_user));
    this.setState({
      user: {
        nombre: JSON_User["nombre"],
        usuario: JSON_User["usuario"],
        clave: JSON_User["clave"],
        correo: JSON_User["correo"],
      },
    });
  }

  guardarUsuario = () => {
    let repetirclave = document.getElementById("repetirclave").value;
    let ds_user = "user_" + this.state.user.usuario;

    if (repetirclave != this.state.user.clave) {
      alert("Las contraseñas no coinciden");
      return;
    }

    localStorage.setItem(ds_user, JSON.stringify(this.state.user));
    alert("Datos Actualizados");
  };

  handleChangeUser = (e) => {
    this.setState({
      user: {
        ...this.state.user,
        [e.target.name]: e.target.value,
      },
    });
  };

  render() {
    return (
      <div className="Content">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-4">
              <img src={LogoImage} alt="Logo" className="img-fluid mb-2" />

              <div className="Center">
                <h2>Hola {this.state.user.nombre}</h2>
              </div>

              <br />
              <Button id="btn_actualizar"
                className="btn btn-info btn-block"
                onClick={this.mostrarModal}
              >
                Actualizar mis datos
              </Button>
              <Link className="btn btn-danger btn-block" to="/">
                Cerrar
              </Link>
            </div>

            <ModalRegister
              user={this.state.user}
              mostrar={this.state.mostrar}
              onChange={this.handleChangeUser}
              guardar={this.guardarUsuario}
              Cerrar={this.mostrarModal}
              Actualizar={"disabled"}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Home);
