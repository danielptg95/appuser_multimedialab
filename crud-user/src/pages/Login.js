import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";

import ModalRegister from "../components/ModalRegister";
import LogoImage from "../logo.svg";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mostrar: false,
      user: {
        nombre: "",
        usuario: "",
        clave: "",
        correo: "",
      },
      login: {
        ds_user: "",
        ds_password: "",
      },
    };
  }

  mostrarModal = () => {
    this.setState({ mostrar: !this.state.mostrar });
  };

  handleChangeUser = (e) => {
    this.setState({
      user: {
        ...this.state.user,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleChangeLogin = (e) => {
    this.setState({
      login: {
        ...this.state.login,
        [e.target.name]: e.target.value,
      },
    });
  };

  guardarUsuario = () => {
    let ds_user = "user_" + this.state.user.usuario;
    localStorage.setItem(ds_user, JSON.stringify(this.state.user));
    alert("Datos Guardados");
  };

  validarUsuario = (e) => {
    let ds_user = "user_" + this.state.login.ds_user;
    let JSON_User = JSON.parse(localStorage.getItem(ds_user));

    this.setState({
      user: {
        nombre: JSON_User["nombre"],
      },
    });

    if (
      this.state.login.ds_user != JSON_User["usuario"] &&
      this.state.login.ds_password != JSON_User["clave"]
    ) {
      alert("Usuario y/o clave invalida");
      e.preventDefault();
    }
  };

  render() {
    return (
      <div className="Content">
        <div className="container">
          {/* logo */}
          <div className="row">
            <div className="col-12 col-md-4">
              <img src={LogoImage} alt="Logo" className="img-fluid mb-2" />
            </div>
          </div>
          {/* campos inicio de sesión */}
          <div className="row">
            <div className="col-12 col-md-4">
              <div className="form-group">
                <label>Usuario</label>
                <input
                  className="form-control"
                  type="text"
                  name="ds_user"
                  value={this.state.login.ds_user}
                  onChange={this.handleChangeLogin}
                />
              </div>
              <div className="form-group">
                <label>Clave</label>
                <input
                  className="form-control"
                  type="password"
                  name="ds_password"
                  maxLength="8"
                  value={this.state.login.ds_password}
                  onChange={this.handleChangeLogin}
                />
              </div>
            </div>
          </div>
          {/* botones */}
          <div className="row">
            <div className="col-12 col-md-4">
              <Link
                className="btn btn-primary btn-block"
                to={`/Home/${this.state.login.ds_user}`}
                onClick={this.validarUsuario}
              >
                Iniciar Sesión
              </Link>
              <Button className="btn btn-secundary btn-block" onClick={this.mostrarModal}>
                Registrarme
              </Button>
            </div>
          </div>
        </div>

        <ModalRegister
          user={this.state.user}
          mostrar={this.state.mostrar}
          onChange={this.handleChangeUser}
          guardar={this.guardarUsuario}
          Cerrar={this.mostrarModal}
          Actualizar={""}
        />
      </div>
    );
  }
}
