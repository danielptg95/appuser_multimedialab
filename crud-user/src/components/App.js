import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import '../App.css';
import Login from '../pages/Login';
import Home from '../pages/Home';
import NotFound from '../pages/NotFound';

function App() {
  return (
    <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/Home/:nombre" component={Home} />
          <Route component={NotFound} />
        </Switch>
    </BrowserRouter>
  );
}

export default App;
