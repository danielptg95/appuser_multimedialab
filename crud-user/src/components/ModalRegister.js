import React from "react";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";

class ModalRegister extends React.Component {

  render() {
    return (
      <Modal isOpen={this.props.mostrar}>
        <ModalHeader>Ingrese sus datos de usuario</ModalHeader>
        <ModalBody>
          <div className="form-group">
            <label>Nombre</label>
            <input
              className="form-control"
              type="text"
              name="nombre"
              value={this.props.user.nombre}
              onChange={this.props.onChange}
            />
          </div>
          <div className="form-group">
            <label>Usuario</label>
            <input
              className="form-control"
              type="text"
              name="usuario"
              value={this.props.user.usuario}
              onChange={this.props.onChange}
              disabled = {(this.props.Actualizar)? "disabled" : ""}
            />
          </div>
          <div className="form-group">
            <label>Clave</label>
            <input
              className="form-control"
              type="password"
              name="clave"
              value={this.props.user.clave}
              onChange={this.props.onChange}
            />
          </div>
          <div className="form-group">
            <label>Repetir Clave</label>
            <input
              className="form-control"
              type="password"
              name="repetirclave"
              id="repetirclave"
            />
          </div>
          <div className="form-group">
            <label>correo</label>
            <input
              className="form-control"
              type="email"
              name="correo"
              value={this.props.user.correo}
              onChange={this.props.onChange}
            />
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.props.guardar}>
            Guardar
          </Button>
          <Button color="secondary" onClick={this.props.Cerrar}>
            Cerrar
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default ModalRegister;
